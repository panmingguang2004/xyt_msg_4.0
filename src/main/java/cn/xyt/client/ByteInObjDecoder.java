package cn.xyt.client;


import cn.xyt.client.ByteInDecoder.Msg;
import cn.xyt.util.ByteUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * SimpleChannelInboundHandler extends ChannelInboundHandlerAdapter
 * 具体业务处理部分
 **/
public class ByteInObjDecoder extends SimpleChannelInboundHandler<Msg>{

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Msg msg) throws Exception {

		// 执行返回
		Msg resp = execMsg(msg);	
	
		// 走到 ByteOutEncoder 
		ctx.channel().write(resp);  
	}

	/**
	 * 处理业务流程
	 * @param msg
	 * @return
	 */
	private Msg execMsg(Msg msg) {
		return null;
	}


}
