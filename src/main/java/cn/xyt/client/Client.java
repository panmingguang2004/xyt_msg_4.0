package cn.xyt.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.xyt.client.ByteInDecoder.Msg;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;

public class Client {
	private static final Logger LOG = LoggerFactory.getLogger(Client.class);
	private static String sn = "test01-by-xin";
	private int port = 7071;
	private String host = "127.0.0.1"; //115.28.35.175
	private SocketChannel socketChannel;

	public Client() {
		try {
			start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void start() throws InterruptedException {
		EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
		Bootstrap bootstrap = new Bootstrap();
		bootstrap.channel(NioSocketChannel.class);
		bootstrap.option(ChannelOption.SO_KEEPALIVE, Boolean.valueOf(true));
		bootstrap.option(ChannelOption.SO_BACKLOG, Integer.valueOf(128));
		bootstrap.group(eventLoopGroup);
		bootstrap.remoteAddress(this.host, this.port);
		bootstrap.handler(new ChannelInitializer() {
			@Override
			protected void initChannel(Channel socketChannel) throws Exception {
				socketChannel.pipeline().addLast("logging", new LoggingHandler(LogLevel.INFO)); 
				socketChannel.pipeline().addLast(new ChannelHandler[] { new IdleStateHandler(0, 0, 0) });
				
				socketChannel.pipeline().addLast(new ChannelHandler[] { new ByteInDecoder()});
				socketChannel.pipeline().addLast(new ChannelHandler[] { new ByteOutEncoder1() });
//				
//				socketChannel.pipeline().addLast(new ChannelHandler[] { new ChannelOutboundHandlerAdapter() });
			}
		});
		ChannelFuture future = bootstrap.connect(this.host, this.port).sync();
		if (future.isSuccess()) {
			this.socketChannel = ((SocketChannel) future.channel());
			LOG.info("连接服务器成功");
		}
	}

	public static void main(String[] args) throws InterruptedException {
		Client bootstrap = new Client();
		heart(bootstrap.socketChannel);
//		for (;;) {
//			
//			Thread.sleep(5000L);
//		}
	}

	
	public static byte[]  str16toByte(String str){
		String cc[] = str.split(" ");
		byte[] bb = new byte[cc.length];
		for(int i=0;i<cc.length;i++){
			bb[i] = (byte)Integer.parseInt(cc[i], 16);
		}
		return bb;
	}
	
	public static void heart(SocketChannel channel) {
		/**
		 * 23 87 11 00 38 22 01 30 30 30 30 30 30 30 30 30 |#...8".000000000|
|00000010| 30 30 30 36 04 00 00 01 10 0b 17 0a 14 20 6c 02 |0006......... l.|
|00000020| 00 00 80 a4 00 c0 00 00 00 07 23              
		 */
		byte [] bb = str16toByte("23 87 11 00 38 22 01 30 30 30 30 30 30 30 30 30 30 30 30 36 04 00 00 01 10 0b 17 0a 14 20 6c 02 00 00 80 a4 00 c0 00 00 00 07 23");
		channel.writeAndFlush(new Msg(bb));
	}
}
