package cn.xyt.client;


import static java.nio.ByteOrder.LITTLE_ENDIAN;

import java.nio.ByteBuffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.MessageToByteEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.xyt.client.ByteInDecoder.Msg;
/**
 * MessageToByteEncoder  extends  ChannelOutboundHandlerAdapter
 * 推荐直接使用 MessageToByteEncoder
 * Encoder 用于发送出去 用于write 
 * 不管是server发送, 还是client 发送,  使用的都是 MessageToByteEncoder.out.write -->ChannelOutboundHandlerAdapter.ctx.writeFlush
 * 
 */
public class ByteOutEncoder extends MessageToByteEncoder<Msg> {
	private static final Logger log = LoggerFactory.getLogger(ByteOutEncoder.class);

	@Override
	protected void encode(ChannelHandlerContext ctx, Msg msg, ByteBuf out) throws Exception {
		out.writeBytes(msg.content);
	}
	
}
