package cn.xyt.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "system")
public class SystemProperty {
	private Integer httpMsgPort;
	private Integer messageServerPort;
	private Integer webRequestTimeout;
	private Integer serverRequestTimeout;
	private Integer deviceHeart;
	private int corePoolSize;
	private int maxPoolSize;
	private int queueCapacity;
	private int keepAliveSeconds;
	private String threadNamePrefix;
	private String DBconfig;
	private String iszc;  //是否租车
	
	
	public Integer getMessageServerPort() {
		return this.messageServerPort;
	}

	public void setMessageServerPort(Integer messageServerPort) {
		this.messageServerPort = messageServerPort;
	}

	public Integer getWebRequestTimeout() {
		return this.webRequestTimeout;
	}

	public void setWebRequestTimeout(Integer webRequestTimeout) {
		this.webRequestTimeout = webRequestTimeout;
	}

	public Integer getDeviceHeart() {
		return this.deviceHeart;
	}

	public void setDeviceHeart(Integer deviceHeart) {
		this.deviceHeart = deviceHeart;
	}

	public int getCorePoolSize() {
		return this.corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaxPoolSize() {
		return this.maxPoolSize;
	}

	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public int getQueueCapacity() {
		return this.queueCapacity;
	}

	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	public int getKeepAliveSeconds() {
		return this.keepAliveSeconds;
	}

	public void setKeepAliveSeconds(int keepAliveSeconds) {
		this.keepAliveSeconds = keepAliveSeconds;
	}

	public String getThreadNamePrefix() {
		return this.threadNamePrefix;
	}

	public void setThreadNamePrefix(String threadNamePrefix) {
		this.threadNamePrefix = threadNamePrefix;
	}

	public Integer getServerRequestTimeout() {
		if (this.serverRequestTimeout != null) {
			this.serverRequestTimeout = Integer.valueOf(this.serverRequestTimeout.intValue() * 1000);
		}
		return this.serverRequestTimeout;
	}

	public void setServerRequestTimeout(Integer serverRequestTimeout) {
		this.serverRequestTimeout = serverRequestTimeout;
	}

	public String getDBconfig() {
		return DBconfig;
	}

	public void setDBconfig(String dBconfig) {
		DBconfig = dBconfig;
	}

	public String toString() {
		return "SystemProperty [messageServerPort=" + this.messageServerPort + ", webRequestTimeout=" + this.webRequestTimeout + ", serverRequestTimeout=" + this.serverRequestTimeout + ", deviceHeart=" + this.deviceHeart + ", corePoolSize=" + this.corePoolSize + ", maxPoolSize=" + this.maxPoolSize + ", queueCapacity=" + this.queueCapacity + ", keepAliveSeconds=" + this.keepAliveSeconds + ", threadNamePrefix=" + this.threadNamePrefix + "]";
	}

	public Integer getHttpMsgPort() {
		return httpMsgPort;
	}

	public void setHttpMsgPort(Integer httpMsgPort) {
		this.httpMsgPort = httpMsgPort;
	}

	public String getIszc() {
		return iszc;
	}

	public void setIszc(String iszc) {
		this.iszc = iszc;
	}
	
}
