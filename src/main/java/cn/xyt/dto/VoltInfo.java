package cn.xyt.dto;

import java.util.Calendar;

public class VoltInfo {
	public int deviceid;
	public float volt;
	public String state1; 
	public float elec;
	public String state2; 
	public short pl;
	public int mlcnt;
	public Calendar c ;
	
	public VoltInfo(int deviceid, float volt, String state1, float elec, String state2, short pl, int mlcnt, Calendar c) {
		this.deviceid = deviceid;
		this.volt = volt;
		this.state1 = state1;
		this.elec = elec;
		this.state2 = state2;
		this.pl = pl;
		this.mlcnt = mlcnt;
		this.c = c;
	}

	public VoltInfo(int deviceid2, double d, int i, int elec2, int j, int k, int mlcnt2, Calendar instance) {
		// TODO Auto-generated constructor stub
	}
	
}
