package cn.xyt.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.tom.db.jdbc.simple.DBUtil;
import cn.tom.kit.IoBuffer;
import cn.tom.kit.io.FileUtil;
import cn.tom.protocol.http.HttpMessage;
import cn.xyt.dto.Form;
import cn.xyt.dto.Message;
import cn.xyt.dto.SystemProperty;
import cn.xyt.util.ByteUtil;
import cn.xyt.util.DeviceCache;
import cn.xyt.util.HttpUtil;
import cn.xyt.util.PullSession;
import cn.xyt.util.WebRequestCache;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
/**
 * http://127.0.0.1:7070/sendcmd?cmds=11&deviceid=123456&code=2,1
 * @author tomsun
 *
 */
@Service
public class CmdService extends BaseService{
	
	@Resource
	SystemProperty systemProperty;

	/**
	 * 机主号码绑定/解绑
	 * @deprecated
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_0E(Form form){
		return form.getResp();
	}
	
	/**
	 * 报警号码绑定/解绑
	 * @deprecated
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_0D(Form form){
		return form.getResp();
	}
	
	
	/**
	 * 重启动命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_0C(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	
	}
	
	/**
	 * 恢复出厂设置
	 * @deprecated
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_0B(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 设置短信报警发送选择命令
	 * @deprecated
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_0A(Form form){
		return form.getResp();
	}
	
	/**
	 * 布防设防
	 * 01 布防		02撤防		03 无 //不改变当前状态，只设定
	 * 01 自动布防		02 手动布防		03不布防
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_10(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		message.setBody(new byte[]{toByte(code[0]),toByte(code[1])});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}

	/**
	 * 启动停止
	 * http://127.0.0.1:7070/sendcmd?cmd=11&deviceid=123456&code=2,1
	 * @param form
	 * 01 启动02 停止03 无 //不改变当前状态，只设定
	 * 01 自动启动02 手动启动
	 * @return
	 */
	public Map<String, Object> cmd_11(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		message.setBody(new byte[]{toByte(code[0]),toByte(code[1])});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 锁/解 电机命令
	 * 01 锁电机 02 解锁电机	
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_12(Form form){
		Message message = getdefaultMessage(form);
		String code = form.getCode();
		message.setBody(new byte[]{toByte(code)});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 远程断电/远程恢复命令
	 * 01 断电 02 恢复
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_13(Form form){

		Message message = getdefaultMessage(form);
		String code = form.getCode();
		message.setBody(new byte[]{toByte(code)});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 解除报警命令命令
	 * 01 解除报警
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_14(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 静音报警/解除静音报警命令
	 * 01 静音报警 02解除静音报警 03 无 //不改变当前状态，只设定
	 * 01 当次有效02 始终有效
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_15(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		message.setBody(new byte[]{toByte(code[0]),toByte(code[1])});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * code 01 最灵敏 ~ 04 最不灵敏
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_16(Form form){
		Message message = getdefaultMessage(form);
		String code = form.getCode();
		message.setBody(new byte[]{toByte(code)});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 寻车信号命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_17(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 关机命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_18(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}

	/**
	 * code = 01 打开蓝牙透传功能		02 关闭蓝牙透传功能
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_19(Form form){
		Message message = getdefaultMessage(form);
		String code = form.getCode();
		message.setBody(new byte[]{toByte(code)});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 *  定时充电
	 *  01：定时充电  02：充电中断 
		0-23（充电启动设置-小时）0-59（充电启动设置-分钟）  //0-23（充电结束设置-小时）--废弃 0-59（充电结束设置-分钟）--废弃
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_1A(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		message.setBody(new byte[]{ByteUtil.BitToByte(code[0]),toByte(code[1]),toByte(code[2]),0,0});
		message.getHeader().setLength((byte)5);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}

	/**
	 * 电压设定
	 *  一共 8个值 
	 * 充电最大电流值（精度0.1）,充电最高电压（精度0.5）, 欠压电压值（精度0.5）, 标称电压值（精度0.5）,充满电压值（精度0.5）
	 * 报警充电电流（精度0.1）,最小充电电流（精度0.1）, 最高使用电压（精度0.5）
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_1B(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		message.setBody(new byte[]{1,toByte(code[0]),toByte(code[1]),toByte(code[2]),toByte(code[3]),
				toByte(code[4]),toByte(code[5]),toByte(code[6]),toByte(code[7])});
		message.getHeader().setLength((byte)9);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 时间设定命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_1C(Form form){
		Message message = getdefaultMessage(form);
		IoBuffer resBody = getLittleBuf();
		resBody.writeByte(1);
		Calendar c = Calendar.getInstance();
		resBody.writeByte(c.get(Calendar.YEAR)-2000);
		resBody.writeByte(c.get(Calendar.MONTH)+1);
		resBody.writeByte(c.get(Calendar.DAY_OF_MONTH));
		resBody.writeByte(c.get(Calendar.HOUR_OF_DAY));
		resBody.writeByte(c.get(Calendar.MINUTE));
		resBody.writeByte(c.get(Calendar.SECOND));
		resBody.flip();
		message.setBody(resBody.readBytes(0, resBody.limit()));
		message.getHeader().setLength((short)message.getBody().length);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	
	/**
	 * 修改蓝牙ID
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_1D(Form form){
		Message message = getdefaultMessage(form);
		String code = form.getCode();
		int len = code.length();
		IoBuffer buffer = IoBuffer.allocate(len +2);
		buffer.writeByte(1);
		buffer.writeByte(len);
		buffer.writeString(code);
		
		message.setBody(buffer.array());
		message.getHeader().setLength((byte)(len +2));
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * code 蓝牙距离值
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_1E(Form form){
		Message message = getdefaultMessage(form);
		String code = form.getCode();
		message.setBody(new byte[]{1,toByte(code)});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 程序升级准备命令
	 * 	1,升级方式00：正常升级01：强制升级02：平台无此版本
	 * 	4,文件名例：1.2.3.4 
	 *	1,大小 01: 256字节02：512字节, 04: 1K字节
	 *  2,总大小
	 * @param form
	 * from.
	 * @return
	 */
	public Map<String, Object> cmd_20(Form form){
		Message message = getdefaultMessage(form);
		IoBuffer resBody = getLittleBuf();
		String [] code = form.getCode().split(",");
		byte cmdid = toByte(code[0]);
		String version = code[1];
		String[] value1=  (version).split("\\.");
		byte [] v =  new byte[]{(byte)Integer.parseInt(value1[0]),(byte)Integer.parseInt(value1[1]) 
				,(byte)Integer.parseInt(value1[2]),(byte)Integer.parseInt(value1[3]) };
		
		Map<String, Object> map = DBUtil.getMap("select * from appconfig where key =?", "deviceversion");
		String filepath = (String)map.get("value2")+"/"+version.substring(0, 5)+"/"+version;
		System.out.println("==cmd_20===filepath=="+filepath);
		byte size = toByte("02"); // 01-256字节 , 02-512字节
		int sbytesize = 512;
		short total =0;
		int crc_32 =0;
		try {
			FileInputStream ff = new FileInputStream(filepath);
			long totalSize = ff.available();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			FileUtil.copy(ff, out);
			crc_32 = (int)ByteUtil.crc32(out.toByteArray()); 
			total = (short) Math.ceil((double)totalSize/sbytesize) ;// 文件长度
		} catch (IOException e) {
			e.printStackTrace();
		}
		resBody.writeByte(cmdid);
		resBody.writeBytes(v);
		resBody.writeByte(size);
		resBody.writeShort(total);
		//  CRC32位
		resBody.writeInt(crc_32);
		resBody.flip();
		message.setBody(resBody.readBytes(0, resBody.limit()));
		message.getHeader().setLength((short)message.getBody().length);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 确认升级
		00：不升级01：升级02：重新发送数据包04：取消升级
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_25(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		String[] value1= code[1].split("\\.");
		
		byte version [] = new byte[]{(byte)Integer.parseInt(value1[0]),(byte)Integer.parseInt(value1[1]) 
				,(byte)Integer.parseInt(value1[2]),(byte)Integer.parseInt(value1[3]) };
		IoBuffer buf = getLittleBuf(5);
		buf.writeBytes(version);
		buf.writeByte(toByte(code[0]));
		message.setBody(buf.array());
		message.getHeader().setLength((byte)5);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	
	}
	
	/**
	 * 开坐垫锁
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_28(Form form){
		Message message = getdefaultMessage(form);
		message.setBody(new byte[]{1});
		message.getHeader().setLength((byte)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * gps上传时间设置
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_29(Form form){
		Message message = getdefaultMessage(form);
		int speed = Integer.parseInt(form.getCode()) ;
		message.setBody(new byte[]{1, (byte)speed});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 设置坐垫锁类型
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_2A(Form form){
		Message message = getdefaultMessage(form);
		String c[] =  form.getCode().split(",");
		message.setBody(new byte[]{(byte) Integer.parseInt(c[0]), (byte)Integer.parseInt(c[1])});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 设置涨闸锁参数
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_2B(Form form){
		Message message = getdefaultMessage(form);
		String c[] =  form.getCode().split(",");
		message.setBody(new byte[]{(byte) Integer.parseInt(c[0]), (byte)Integer.parseInt(c[1])});
		message.getHeader().setLength((byte)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * 综合设置
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_2C(Form form){
		Message message = getdefaultMessage(form);
		String code[] = form.getCode().split(",");
		byte [] bb = new byte[16];
		for(int i=0; i<bb.length; i++){
			bb[i] = toByte(code[i]);
		}
		message.setBody(bb);
		message.getHeader().setLength((short)16);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * LED 透传
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_2E(Form form){
		Message message = getdefaultMessage(form);
		String [] code = form.getCode().split(",");
		byte[] content = code[1].getBytes(Charset.forName("GBK"));
		IoBuffer buffer = IoBuffer.allocate(content.length+1);
		buffer.writeByte(Integer.parseInt(code[0]));
		buffer.writeBytes(content);
		message.setBody(buffer.array());
		message.getHeader().setLength((short)(content.length+1));
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}

	/**
	 * LED 灯箱控制命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_2F(Form form){
		Message message = getdefaultMessage(form);

		String code[] = form.getCode().split(",");
		IoBuffer body =  getLittleBuf(12);
		body.writeByte(ByteUtil.BitToByte(code[0]));
		body.writeByte(0);
		body.writeBytes(ByteUtil.hexStrtoByte(code[1]));
		message.setBody(body.array());
		message.getHeader().setLength((short)12);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * 设置防盗器夜间模式, 防止扰民
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_30(Form form){
		Message message = getdefaultMessage(form);
		String params[] =  form.getCode().split(",");
		
		byte[] body = new byte[]{
				(byte) Integer.parseInt(params[0]),
				(byte) Integer.parseInt(params[1]), (byte) Integer.parseInt(params[2]), 0,
				(byte) Integer.parseInt(params[3]), (byte) Integer.parseInt(params[4]), 0,
				(byte) Integer.parseInt(params[5])};
		message.setBody(body);
		message.getHeader().setLength((short)8);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 注销 或 设置 锂电池命令
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_31(Form form){
		Message message = getdefaultMessage(form);
		byte[] body = new byte[]{ByteUtil.BitToByte(form.getCode())};
		message.setBody(body);
		message.getHeader().setLength((short)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	/**
	 * 
	 * 电动车控制器设置命令 具体查看 文档
	 * 0：无1：一键修复控制器	
	 * 0：无	1：取消限速值	
	 *0：无1：设置限速值	
	 *0：无1：解锁控制器	
	 *0：无1：锁住控制器
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_32(Form form){
		Message message = getdefaultMessage(form);
		String code [] =  form.getCode().split(",");
		byte[] body = new byte[]{ByteUtil.BitToByte(code[0]), (byte)Integer.parseInt(code[1], 16) };
		message.setBody(body);
		message.getHeader().setLength((short)2);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * 套套机命令 
	 * 01-出套套  02-打开盒子补货 
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_33(Form form){
		Message message = getdefaultMessage(form);
		int code  =  Integer.parseInt(form.getCode()) ;
		byte[] body = new byte[]{(byte)code};
		message.setBody(body);
		message.getHeader().setLength((short)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/**
	 * 限速
	 * 00: 取消限速
	   01：设置限速
	 * @param form
	 * @return
	 */
	public Map<String, Object> cmd_35(Form form){
		Message message = getdefaultMessage(form);
		int code  =  Integer.parseInt(form.getCode()) ;
		byte[] body = new byte[]{(byte)code};
		message.setBody(body);
		message.getHeader().setLength((short)1);
		message.intCheck();
		/*发送消息*/
		sendMessageAsync(message, form);
		/* await end*/
		return form.getResp();
	}
	
	
	/************** 新增协议位置*******************/
	/**
	 * 同步回复
	 * @param msg
	 */
	public void resp_comm(Message msg){
		byte[] body = msg.getBody();
		int state = body[1];
		String res = FAILD;
		if(state == 1){
			res = SUCCESS;
		}
		String key = msg.getHeader().getWebEntrityKey();
		Form form = WebRequestCache.get(key);  // 超期后,已经remove掉, 返回form可能为 null
		if(form == null) return; 
		form.setResp(correct(res));
		form.release();
		WebRequestCache.remove(key);
	};
	
	/**
	 * 异步回复
	 * @param msg
	 */
	public void resp_comm_async(Message msg){
		byte[] body = msg.getBody();
		int state = body[1];
		String res = FAILD;
		if(state == 1){
			res = SUCCESS;
		}
		String key = msg.getHeader().getWebEntrityKey();
		PullSession ps = WebRequestCache.get(key);  // 超期后,已经remove掉, 返回form可能为 null
		if(ps !=null){
			HttpMessage httpmsg = ps.getPullMessage();
			httpmsg.setJsonBody(HttpUtil.serialize(correct(res)));
			httpmsg.setStatus("200");  
			ps.getSession().write(httpmsg);
			WebRequestCache.remove(key);
		}
	};
	
	/**
	 * 异步发送
	 * @param message
	 * @param form
	 */
	private String sendMessageAsync(final Message message, final Form form) {
		final Channel channel = DeviceCache.get(form.getDeviceid());
		if(channel == null) {
			form.setResp(error("设备不在线"));
			logger.info("设备不在线 id={}", form.getDeviceid());
			return null;
		}
		channel.writeAndFlush(message);
		String webcashkey = message.getHeader().getWebEntrityKey();
		form.setResp(correct("发送命令中", webcashkey));
		int deviceid = message.getHeader().getDeviceid();
		logger.info("sendMessage already, cmd:{} code:{}, id:{}",  form.getCmd(),form.getCode() , deviceid);
		return webcashkey;
	}
	
	
	
	/**
	 * 发送消息并等待返回
	 * @param message
	 * @param form
	 */
	private void sendMessageAwait(final Message message, final Form form) {
		final Channel channel = DeviceCache.get(form.getDeviceid());
		if(channel == null) {
			form.setResp(error("设备不在线"));
			logger.info("设备不在线 id={}", form.getDeviceid());
			return;
		}

		ChannelFuture future = channel.writeAndFlush(message);
		future.addListener(new ChannelFutureListener() {  // 添加一个监听
			@Override
			public void operationComplete(ChannelFuture _future) throws Exception {
				int deviceid = message.getHeader().getDeviceid();
				if(_future.isSuccess()){
					/* 发送成功后加入缓存, 定时过期 释放,  设备id+命令id+[序列号] 没有序列号, 同一个命令多次会被覆盖  */
					String webcashkey = message.getHeader().getWebEntrityKey();
					WebRequestCache.set(webcashkey, form);
					logger.info("sendMessage Success, cmd:{} code:{}, id:{}",  form.getCmd(),form.getCode() , deviceid);
				}else{
					logger.info("sendMessage Faild, cmd:{} code:{} id:{}",  form.getCmd() ,form.getCode(), deviceid);
				}
				// 卡住线程等待
				form.getCdl().await(CmdService.this.systemProperty.getWebRequestTimeout().intValue(), TimeUnit.SECONDS);
				WebRequestCache.remove(message.getHeader().getWebEntrityKey());
			}
		});
	}
	

	
	
	public static void main(String[] args) {
		short a = (short) (System.currentTimeMillis()/10000000);
		System.out.println(System.currentTimeMillis());
		System.out.println(Short.MAX_VALUE);
		System.out.println(a);
	}
}
