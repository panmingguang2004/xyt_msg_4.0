package cn.xyt.server;


import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import cn.xyt.dto.Header;
import cn.xyt.dto.Message;
import cn.xyt.dto.SystemProperty;
import cn.xyt.service.CmdService;
import cn.xyt.service.MessageService;
import cn.xyt.service.MessageServiceByZc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 接收 消息的 控制 端, 包括 上报的消息 和 命令返回消息
 * @author tomsun
 *
 */
@Service
public class MessageController {
	@Autowired
	private SystemProperty property;
	MessageService messageService;
	@Resource
	CmdService cmdService;
	
	public static Map<Integer, Object[]> msgid_exe = new HashMap<>();
	{
		try {
			/* 直接 上报数据*/
			//注册
			msgid_exe.put(0x8F, new Object[]{0x0F, MessageService.class.getMethod("register", Message.class)});
			msgid_exe.put(0x81, new Object[]{0x01, MessageService.class.getMethod("login", Message.class)}); //终端鉴权
			msgid_exe.put(0x82, new Object[]{0x02, MessageService.class.getMethod("heart", Message.class)}); //心跳
			msgid_exe.put(0x83, new Object[]{0x03, MessageService.class.getMethod("diviceInfo", Message.class)}); //基本信息上报
			msgid_exe.put(0x84, new Object[]{0x04, MessageService.class.getMethod("MCU", Message.class)}); //MCU数据透传
			//msgid_exe.put(0x85, new Object[]{0x05, MessageService.class.getMethod("login", Message.class)}); //蓝牙数据透传
			msgid_exe.put(0x86, new Object[]{0x06, MessageService.class.getMethod("warnInfo", Message.class)}); //报警汇报
			msgid_exe.put(0x87, new Object[]{0x07, MessageService.class.getMethod("voltInfo", Message.class) }); //电瓶状态信息上报
			//msgid_exe.put(0x9D, new Object[]{0x1D }); //上报蓝牙ID
			msgid_exe.put(0xC0, new Object[]{0x40, MessageService.class.getMethod("batchwarnInfo", Message.class)}); //存储的报警信息上报
			msgid_exe.put(0xC1, new Object[]{0x41, MessageService.class.getMethod("batchvoltInfo", Message.class)}); //存储的电瓶状态信息上报
			msgid_exe.put(0xA1, new Object[]{0x21, MessageService.class.getMethod("uppackage", Message.class)}); //申请升级数据包
			msgid_exe.put(0XA4, new Object[]{0X24, MessageService.class.getMethod("checkupdate", Message.class) }); //数据包接收完成,上传校验状态
			msgid_exe.put(0XA6, new Object[]{0x26, MessageService.class.getMethod("checkversion", Message.class) }); 
			msgid_exe.put(0xAD, new Object[]{0x2D, MessageService.class.getMethod("simcaid", Message.class) }); 
			msgid_exe.put(0xB0, new Object[]{0x30, MessageService.class.getMethod("errmsg", Message.class) }); 
			
			// 套套机故障
			msgid_exe.put(0xB4, new Object[]{0x34, MessageService.class.getMethod("ttstate", Message.class) }); 
			
			/********************** 服务器cmd 命令返回 处理 关系*****************************/
			Object[] method =  new Object[]{CmdService.class.getMethod("resp_comm_async", Message.class)};
			msgid_exe.put(0x8E, method); //机主号码绑定/解绑
			msgid_exe.put(0x8D, method); //报警号码绑定/解绑
			msgid_exe.put(0x8C, method); //重启动命令
			msgid_exe.put(0x8B, method); //恢复出厂设置命令
			msgid_exe.put(0x8A, method); //设置短信报警发送选择命令
			msgid_exe.put(0x90, method); //布防/撤防命令
			msgid_exe.put(0x91, method); //启动/停止命令
			msgid_exe.put(0x92, method); //锁/解 电机命令

			msgid_exe.put(0x93, method); //远程断电/远程恢复命令
			msgid_exe.put(0x94, method); //解除报警命令命令
			msgid_exe.put(0x95, method); //静音报警/解除静音报警命令
			msgid_exe.put(0x96, method); //报警灵敏度设置命令
			msgid_exe.put(0x97, method); //寻车信号命令
			msgid_exe.put(0x98, method); //关机命令
			msgid_exe.put(0x99, method); //蓝牙透传命令
			msgid_exe.put(0x9A, method); //定时充电命令
			msgid_exe.put(0x9B, method); //电瓶电压设定命令
			msgid_exe.put(0x9C, method); //时间设定命令
			msgid_exe.put(0x9D, method); //修改蓝牙ID
			msgid_exe.put(0x9E, method); //蓝牙距离设定命令
			
			msgid_exe.put(0xA0, method); //程序升级准备命令
			msgid_exe.put(0xA5, method); //程序升级确认命令
			
			msgid_exe.put(0xAA, method); //坐垫锁设置
			msgid_exe.put(0xAB, method); //坐垫锁设置
			msgid_exe.put(0xA8, method); //坐垫锁发送命令
			msgid_exe.put(0xA9, method); //gps 上传时间设置
			msgid_exe.put(0xAC, method); //综合设置返回
			
			//LED 透传命令 返回
			msgid_exe.put(0xAE, method); 
			msgid_exe.put(0xAF, method);
			
			msgid_exe.put(0xB0, method); // 防止扰民参数
			msgid_exe.put(0xB1, method); // 电池类型,app注销返回 
			msgid_exe.put(0xB2, method); // 电动车控制器设置命令返回 

			msgid_exe.put(0xB3, method); 
			msgid_exe.put(0xB5, method); 
 
			
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} 
	}
	
	public boolean exec(Message msg) throws Exception{
		Header header = msg.getHeader();
		int msgid = (byte)header.getMsgid() & 0xFF;
		Object [] obj = msgid_exe.get(msgid);
		Method me = null;
		
		if(obj.length == 2){
			/*设置返回msgid*/
			int rmsgid = (int)obj[0];
			header.setMsgid((byte)rmsgid); 
			
			/*返回方法调用*/
			me = (Method) obj[1]; 
			/*获取返回 body */
			byte[] resBody = null;
			try{
				resBody =(byte[]) me.invoke(messageService, msg);
			}catch(Exception e){
				messageService.logger.error("exec -->", e);
			}
			msg.setBody(resBody);
			
			/* 重新调整 header 的长度 */
			msg.getHeader().setLength(msg.getBody()!=null ? (short)msg.getBody().length : 0);
			
			/*生成校验码*/
			msg.intCheck();
			return true;
			
		}else{
			// 处理 命令回复消息, 不需要再回复给 设备
			me = (Method) obj[0]; 
			me.invoke(cmdService, msg);
		}
		return false;
		
	}
	
	

	@PostConstruct
	void init(){
		if("1".equals(property.getIszc())){
			messageService = new MessageServiceByZc();
			System.out.println("messageService::"+messageService);
		}else{
			messageService = new MessageService();
		}
	}
	
}
