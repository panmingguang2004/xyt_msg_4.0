package cn.xyt.net;

import java.io.IOException;
import java.util.Map;

import cn.tom.protocol.http.HttpMessage;
import cn.tom.protocol.http.HttpMessageAdaptor;
import cn.tom.transport.nio.NioClient;

/**
 * 测试 CmdController 的类
 * @author tomsun
 *
 */
public class Client {
	
	
	public static void main(String[] args) throws IOException, InterruptedException {

		NioClient<HttpMessage> client = new NioClient("121.42.247.56:7070");  //121.42.247.56
		client.setIoAdaptor(new HttpMessageAdaptor());
		HttpMessage msg = new HttpMessage();
		msg.setCommand("get"); //sendcmd
		//params.get("deviceid")).set("code", "1,1").set("msgid", "11")
		msg.getMeta().setParam("module", "queryone");
		msg.getMeta().setParam("sql", "select  string_agg(did::text,',') dids from (select did from gpsone a  where "
				+ "earth_box(ll_to_earth(122.0,36.0),3000) @> ll_to_earth(a.lat, a.lon) limit 100)s  ");
		msg.setBody("");
		
		long a = System.currentTimeMillis();
		msg = client.invokeSync(msg);
		System.out.println(System.currentTimeMillis() -a);
		System.out.println("======" +msg);
		
	
	}

}
