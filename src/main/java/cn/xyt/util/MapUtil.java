package cn.xyt.util;

/**
 * 火星坐标 转 高德坐标系
 * 百度坐标拾取器: http://api.map.baidu.com/lbsapi/getpoint/index.html
	高德: http://lbs.amap.com/console/show/picker
 * @author tomsun
 *
 */
public class MapUtil {
	static double pi = 3.14159265358979324;
	static double a = 6378245.0;
	static double ee = 0.00669342162296594323;
	public final static double x_pi = 3.14159265358979324 * 3000.0 / 180.0;

	/**
	 *  wgs-84 转 bd(百度)
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static double[] wgs2bd(double lat, double lon) {
	       double[] wgs2gcj = wgs2gcj(lat, lon);
	       double[] gcj2bd = gcj2bd(wgs2gcj[0], wgs2gcj[1]);
	       return gcj2bd;
	}

	public static double[] gcj2bd(double lat, double lon) {
	       double x = lon, y = lat;
	       double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
	       double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
	       double bd_lon = z * Math.cos(theta) + 0.0065;
	       double bd_lat = z * Math.sin(theta) + 0.006;
	       return new double[] { bd_lat, bd_lon };
	}

	public static double[] bd2gcj(double lat, double lon) {
	       double x = lon - 0.0065, y = lat - 0.006;
	       double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
	       double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
	       double gg_lon = z * Math.cos(theta);
	       double gg_lat = z * Math.sin(theta);
	       return new double[] { gg_lat, gg_lon };
	}

	/**
	 * wgs-84 转 gcj-02(高德)
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static double[] wgs2gcj(double lat, double lon) {
	       double dLat = transformLat(lon - 105.0, lat - 35.0);
	       double dLon = transformLon(lon - 105.0, lat - 35.0);
	       double radLat = lat / 180.0 * pi;
	       double magic = Math.sin(radLat);
	       magic = 1 - ee * magic * magic;
	       double sqrtMagic = Math.sqrt(magic);
	       dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
	       dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
	       double mgLat = lat + dLat;
	       double mgLon = lon + dLon;
	       double[] loc = { mgLat, mgLon };
	       return loc;
	}

	private static double transformLat(double lat, double lon) {
	       double ret = -100.0 + 2.0 * lat + 3.0 * lon + 0.2 * lon * lon + 0.1 * lat * lon + 0.2 * Math.sqrt(Math.abs(lat));
	       ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
	       ret += (20.0 * Math.sin(lon * pi) + 40.0 * Math.sin(lon / 3.0 * pi)) * 2.0 / 3.0;
	       ret += (160.0 * Math.sin(lon / 12.0 * pi) + 320 * Math.sin(lon * pi  / 30.0)) * 2.0 / 3.0;
	       return ret;
	}

	private static double transformLon(double lat, double lon) {
	       double ret = 300.0 + lat + 2.0 * lon + 0.1 * lat * lat + 0.1 * lat * lon + 0.1 * Math.sqrt(Math.abs(lat));
	       ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
	       ret += (20.0 * Math.sin(lat * pi) + 40.0 * Math.sin(lat / 3.0 * pi)) * 2.0 / 3.0;
	       ret += (150.0 * Math.sin(lat / 12.0 * pi) + 300.0 * Math.sin(lat / 30.0 * pi)) * 2.0 / 3.0;
	       return ret;
	}
	
	/**
     * nmea-0183 
     * GPRMC 格式经纬度ddmm.mmmm(度分)格式 转wgs84 小数格式 
     * @param lat
     * @param lon
     * @return
     * 3150.1835 11956.7629
     */
    public static double[]  gprmc2wgs84 (double lat, double lon){
    	// 3150.1835  11956.7629
		int lat_d = (int)lat/100; //31
		int lon_d = (int)lon/100; //119
		
		int lat_f = (int) (lat -lat_d*100); //50
		double lat_f1 = lat_f/60D; //0.83333333
//		System.out.println("lat_f1==="+lat_f1);
		double lat_s = (lat -(int)lat)/60;
		
		int lon_f = (int) (lon -lon_d*100); //56
		
		double lon_f1 = lon_f/60D; //0.9333333333
//		System.out.println("lon_f1==="+lon_f1);
		double lon_s = (lon- (int)lon)/60;
		
		return new double[]{lat_d+lat_f1+lat_s, lon_d+lon_f1+lon_s};
    }
	
	public static void main(String[] args) {
		// 11956.7537-119.945895, 3150.1737-31.836228333333334
		////lon=11956.7537, batvol=39.6, safeStatus=0, deviceStatus=1, lat=3150.1737}
		
		//3150.1835 11956.7629
		//11956.7714, 3150.1641
//		double[] wgs = gprmc2wgs84(3150.1767 ,11956.7537);
//		System.out.println(wgs[0]+","+wgs[1]);
		//120.012533333333,31.7233666666667 
		//119.9646837717331,31.6829978476698
		double[] wgs2gcj = wgs2gcj(31.6829978476698 , 119.9646837717331);
		System.out.println(wgs2gcj[1]+","+wgs2gcj[0]);
		double[] gcj2bd = gcj2bd(wgs2gcj[0], wgs2gcj[1]);
		System.out.println(gcj2bd[1]+","+gcj2bd[0]);
		
	}

}
