package cn.xyt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;



//@ServletComponentScan // 使用servlet3.0 annotation 检索, 用来配置 druid 监控
@SpringBootApplication
@EnableConfigurationProperties({ cn.xyt.dto.SystemProperty.class })
public class Application {
	public static void main(String[] args) throws ClassNotFoundException {
		SpringApplication.run(Application.class, args);
		
//		while(true){
//			JPushUtil.takeGpsResult(3, "31.680997,119.968831");
//			try {
//				Thread.sleep(3000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}
}




